<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Email Init Config
|--------------------------------------------------------------------------
|
|
*/
$config['protocol'] = 'mail';                       //mail, sendmail, or smtp
$config['charset'] = 'uft-8';
$config['wordwrap'] = TRUE;
$config['mailtype'] = 'html';
$config['smtp_host'] = '';
$config['smtp_user'] = '';
$config['smtp_pass'] = '';
$config['smtp_port'] = 25;
$config['newline'] = "\r\n";

/* End of file email.php */
/* Location: ./application/config/email.php */