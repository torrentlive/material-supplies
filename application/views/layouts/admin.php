<?= doctype('xhtml1-trans'); ?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    
    <? $this->view->title(); ?>    
    <? $this->view->metas(); ?>
    <? $this->view->css(array( 'admin/reset', 'admin/root', 'admin/grid', 'admin/typography', 'admin/jquery-ui', 'admin/jquery-plugin-base')); ?>    
    <? $this->view->js( array( 'admin/jquery-1.7.1.min', 'admin/jquery-ui-1.8.18.custom.min', 'admin/jquery-settings', 'admin/toogle', 'admin/jquery.tipsy', 'admin/jquery.uniform.min', 'admin/jquery.wysiwyg', 'admin/raphael', 'admin/analytics', 'admin/popup', 'admin/fullcalendar.min', 'admin/jquery.prettyPhoto', 'admin/jsapi', 'admin/jquery.dataTables', 'ckeditor/ckeditor', 'ckfinder/ckfinder', 'ckeditor/adapters/jquery')); ?>    
    <style>#stats{display:block;}</style>          
    
</head>
<body>

<div class="wrapper">
    
	<!-- START HEADER -->
    <div id="header">
    	<!-- logo -->
    	<div class="logo">	
            <a href="index.html">
                <?= img( array( 'src' => '/images/admin/logo.png', 'style' => 'width: 112px; height: 35px', 'alt' => 'logo')); ?>
            </a>	
        </div>        
        <!-- profile box -->
        <div class="profilebox-top">
        	<a href="<?= admin_profile_url(); ?>" class="display">
                <span><?= $this->session->userdata( 'username'); ?></span>
            </a>
            <a href="/logout" class="display">
                <span>Sign out</span>
            </a>
        </div>
        <div class="clear"></div>
    </div>
<!-- START MAIN -->
    <div id="main">
            <div id="top-menu" class="top-menu">
            <ul class="top-menu">
                <li>
                    <a href="#">menu</a>
                </li>
                <li>
                    <a href="#">menu</a>
                </li>
                <li>
                    <a href="#">menu</a>
                </li>
            </ul>
        </div>
<table style="width: 100%;">
    <tr>
        <td class='top-menu'>
            <? $this->view->partial('common/_admin_menu'); ?>
        </td>
        <!-- END HEADER -->
        <td>
            <?= $yield; ?>
        <div class="clear"></div>
        </td>
    </tr>
</table>
    </div>
    <!-- END MAIN -->
        
    <!-- START FOOTER -->

    <div id="footer">
    	<div class="left-column">&copy; Copyright 2012 - All rights reserved.</div>
        <div class="right-column"><a href="http://www.webstartin.com" target="_blank"><?= img( array( 'src' => '/images/webstartin-mini.png', 'style' => 'border: 0px')) ?></a></div>
    </div>
    <!-- END FOOTER -->

</div>
</body>
</html>