<?

/**
 * @author ianlop iamlop@imagesum.net 
 * 
 * Ex
 *      var $uploads = array(
 *         'fieldname' => array(
 *             'extensions' => 'gif|jpg|png',
 *             'resize' => array(
 *                 'filename' => array(
 *                     'width' => 100,
 *                     'height' => 100,
 *                     'quality' => '90%'
 *                 ),
 *                 'filename' => array(
 *                     'width' => 300,
 *                     'height' => 300,
 *                     'quality' => '90%'
 *                 )
 *             )           
 *         ),
 *         'file2' => array(
 *             'extensions' => 'doc|docx|pdf',
 *         ),
 *     ); 
 *     
 */

class DMZ_Upload {
    
    var $row_path;
    var $current_field;
    var $current_result;
    var $oldExtension;
    
    /**
     * 
     */
    function __construct(){  
        $this->CI =& get_instance();        
        $this->CI->load->library( 'upload');
        $this->CI->load->library('image_lib');
    }
    
    /**
     * 
     */
    function save_upload( $object, $data = array(), $related = ''){

        if( empty( $data))
            return;
        
        if( !empty( $data['id']))
            $object->get_by_id( $data['id']);

        $object->from_array( $data);
        
        //Add slug
        $object->addSlug();        
        
        $object->save( $related);     

        foreach( $object->uploads as $key => $upload){
            unset( $result_upload);
            unset( $this->oldExtension);
            
            $model_dir = APPPATH . 'assets/uploads/'. ucfirst($object->model);
            $this->row_path = $model_dir .'/'. $object->id;
            $this->current_field = $key;
            $this->oldExtension = $object->{$key};
            
            //Delete File file checkbox checked
            if( !empty($data[ $this->current_field .'_delete']))
                $object->deleteRecord( $this->current_field);
            
            //Check and create dir
            if( !is_dir( $model_dir)) mkdir( $model_dir);       
            if( !is_dir( $this->row_path)) mkdir( $this->row_path);
            
            //Upload
            $result_upload = $object->doupload( $upload['extensions']);
            if( $result_upload['file_size'] != 0){
                $object->{$key} = $result_upload['file_ext'];
                $this->current_result = $result_upload;
                
                //Delete old file if extension diff type
                if( $result_upload['file_ext'] != $this->oldExtension)
                    $object->deletefile( $this->current_field);
                    
                //resize version    
                if( !empty( $upload['resize']) && $result_upload['image_type']){
                    foreach( $upload['resize'] as $key_name => $resize){
                        $object->copy_resize( $key_name, $resize);
                    }
                }
            }            
        }        
        return $object->save();
    }

    /**
     * 
     */
    function doupload( $object, $extensions){        
        $config['upload_path'] = $this->row_path;
        $config['allowed_types'] = $extensions;
        $config['file_name'] = $this->current_field;
        $config['overwrite'] = true;
        
        $this->CI->upload->initialize( $config);
        $this->CI->upload->do_upload( $this->current_field);
        return $this->CI->upload->data();
    }
    
    /**
     * 
     */
    function copy_resize( $object, $namefile, $new_image){
        
        $filename = $this->current_result['raw_name'] .'_'. $namefile;
        $object->deletefile( $filename);
        
        $config['image_library'] = 'gd2';
        $config['new_image'] =  $filename . $this->current_result['file_ext'];
        $config['source_image'] = $this->current_result['full_path'];
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $new_image['width'];
        $config['height'] = $new_image['height'];
        $config['quality'] = $new_image['quality'];

        $this->CI->image_lib->clear();
        $this->CI->image_lib->initialize( $config);
        $this->CI->image_lib->resize();
    }
    
    /**
     * 
     */
    function get_upload( $object){
        
        $object->get();
        
        if( $object->uploads)
        foreach( $object->uploads as $key => $upload){ 
            if( $object->{$key}){
                $extension = $object->{$key};
                if( !is_array( $object->{$key})) $object->{$key} = array();
                
                $object->{$key}['url'] = '/uploads/'. ucfirst($object->model) .'/'. $object->id .'/'. $key . $extension; 
                
                foreach( $upload['resize'] as $key_size => $size){
                    $object->{$key}[$key_size] = '/uploads/'. ucfirst($object->model) .'/'. $object->id .'/'. $key .'_'. $key_size . $extension; 
                }
            }            
        }
        
        return clone $object;        
    }
    
    /**
     * 
     */
    function delete_upload( $object, $id = ''){
        
        if( $id)
            $object->id = $id;
            
        $id = $object->id;        
        
        if( $object->delete()){
            $model_dir = APPPATH . 'assets/uploads/'. ucfirst($object->model);
            $this->row_path = $model_dir .'/'. $id;
            delete_files( $this->row_path, true);
            rmdir( $this->row_path);
            return true;
        }        
        
        return false;
        
    }
    
    /**
     * 
     */
    function deletefile( $object, $filename){ 
        if( is_file( $this->row_path .'/'. $filename .  $this->oldExtension))
            @unlink( $this->row_path .'/'. $filename .  $this->oldExtension);
    }
    
    /**
     * 
     */
    function deleteRecord( $object, $filename){        
        $object->deletefile( $filename);
        
        if( $object->uploads[$filename]['resize'])
        foreach( $object->uploads[$filename]['resize'] as $key => $data)
            $object->deletefile( $filename .'_'. $key);
            
        $this->oldExtension = '';
        $object->{$filename} = '';
    }
    
    /**
     * 
     */
    function addSlug( $object){
        if( !$object->slug)
            return;
            
        foreach( $object->slug as $field => $slug){
        	$clean = strtolower(trim( $object->{$field}, '-'));		
			$clean = preg_replace("`\[.*\]`U","",$clean);
			$clean = preg_replace('`&(amp;)?#?[a-z0-9]+;`i','-',$clean);
			$clean = str_replace('%', '-percent', $clean);
			$clean = htmlentities( $clean, ENT_COMPAT, 'utf-8');
			$clean = preg_replace( "`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i","\\1", $clean );
			$clean = preg_replace( array("`[^a-z0-9ก-๙เ-า]`i","`[-]+`") , "-", $clean);
			
            $object->{$slug} = $clean;
        }
 
    }
    

}
?>