<?
/**
 * @name MY_Controller
 * @author iamlop (iamlop@imageslum.net)
 */

class MY_Controller extends CI_Controller{
    
    var $global_uses = array();
    var $uses = array();
    var $helpers = array();
    var $allowPage = array();
    var $_data = array();
    var $data;
       
    function __construct(){
        parent::__construct();  
        $this->_allowPage();
        $this->_loadAllModels();
        $this->_createModelObject(); 
        $this->_loadHelper();
        $this->FlashMessage();
        $this->setData(); 
                
        $this->view->config( array( 'metas' => array( 'https' => array( 'content-type' => 'text/html; charset='. $this->config->item('charset')))));
    }
    
    /**
     * 
     */
     function _allowPage(){
        $array = $this->allowPage;
        $CI = &get_instance();
        $current_method = $this->router->method;
        
        if( preg_match( '/^admin_/', $current_method, $match)){
            if( !$CI->tank_auth->is_role('admin')){
                $this->FlashMessage( 'Permission Deny', 'error');
                redirect( '/');
            }    
        }else{
            //Check user for allow page
            if( count( $array) > 0)
            if( !in_array( $current_method, $array) && $CI->tank_auth->get_role() == ''){   
                $this->FlashMessage( 'Permission Deny', 'error');
                redirect( '/');            
            }
        }
     }
    
    /**
     * @name _loadAllModels
     * @todo Auto scan models in all module
     * @param null
     * @return null
     */
     function _loadAllModels(){
        $CI = &get_instance();
        $this->uses = array_unique(array_merge( $this->uses, $this->global_uses));
        $module_path = $CI->config->config['modules_locations']['application/modules/'];
        $array_model = array();
        $all_list = scandir( APPPATH.'modules');
        foreach( $all_list as $current_get){
            if( is_dir( APPPATH.'modules/'.$current_get . '/models')){
                if( $current_get == '.' || $current_get == '..')
                    continue;
                array_push( $array_model, APPPATH.'modules/'.$current_get);
            }
        }
        Datamapper::add_model_path( $array_model);
     }
    
    /**
     * @name    _createModelObject
     * @todo    Create Model Object automatic
     * @param   null
     * @return  null
     */
     function _createModelObject(){
        foreach( $this->uses as $th_model)
            $this->{$th_model} = new $th_model(); 

     }
     
     /**
      * @name _loadHelper
      * @todo load helper
      * @param null
      * @return null
      */
      function _loadHelper(){
        $this->load->helper( $this->helpers);
      }
      
      /**
       * 
       */
       function FlashMessage( $message = null, $type = null){
            if( $message != null){
                $this->session->set_flashdata( array('FlashMessage' => $message, 'Type' => $type));
            }else{
                $typebox = $this->session->flashdata('Type');
                if( empty( $typebox)) return '';
                $typebox .= 'box';
                $this->_data['FlashMessage'] = '<div class="albox '. $typebox .'" style="z-index: 820;"> <b>'. ucfirst( $type ) .'</b> '. $this->session->flashdata('FlashMessage') .'<a class="close tips" href="#" original-title="close">close</a></div>';

            }
       }
       
       /**
        * 
        */
        function setData(){
            if( $this->input->post( 'data'))
                $this->data = $this->input->post( 'data');
          
        }
  
//    
}
?>