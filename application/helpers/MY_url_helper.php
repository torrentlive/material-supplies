<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function main_url($full = false){
    $url = '/';
    return ( $full == true) ? $site_url($url) : $url;    
}

function admin_url($full = false){     
    $url = '/admin';
    return ( $full == true) ? $site_url($url) : $url;    
}

function admin_profile_url($full = false){
    $url = admin_url() .'/profile';
    return ( $full == true) ? $site_url($url): $url;    
}

function logout_url($full = false){     
    $url = '/logout';
    return ( $full == true) ? $site_url($url) : $url;    
}

//---------------------- User ------------------------

function admin_user_url($full = false){  
    $url = admin_url() .'/users/users';
    return ( $full == true) ? $site_url($url) : $url;    
}

function admin_user_modify_url($id, $full = false){ 
    $url = admin_url() .'/users/users/modify/'. $id;
    return ( $full == true) ? $site_url($url) : $url;    
}

function admin_category_url($full = false){  
    $url = admin_url() .'/categories/categories';
    return ( $full == true) ? $site_url($url) : $url;    
}



?>