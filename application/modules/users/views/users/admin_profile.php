        <!-- START PAGE -->
        <div id="page">           	
                <? $this->view->partial( 'common/_admin_page_title'); ?>
                <!-- START CONTENT -->
                <div class="content">

                        <!-- START SIMPLE FORM -->
                        	<div class="simplebox grid98p">
                            	<div class="titleh">
                                	<h3>Administrator Password</h3>
                                    <div class="shortcuts-icons"></div>
                                </div>
                                <div class="body">
                                    <?php form_open( null, array( 'id' => 'form_submit')); ?>
                                  	<div class="st-form-line">	
                                    	<span class="st-labeltext">Old Password</span>	
                                        <?php form_password( array( 'name' => 'data[oldpassword]', 'class' => 'st-forminput', 'id' => 'oldpassword', 'style' => 'width: 200px', 'required' => 'true')) ?>
                                    <div class="clear"></div>
                                    </div>
                                                                        
                                  	<div class="st-form-line">	
                                    	<span class="st-labeltext">New Password</span>	
                                        <?php form_password( array( 'name' => 'data[password]', 'class' => 'st-forminput', 'id' => 'password', 'style' => 'width: 200px', 'required' => 'true')) ?>
                                    <div class="clear"></div>
                                    </div>
                                    
                                    <div class="st-form-line">	
                                    	<span class="st-labeltext">New Password Again</span>	
                                        <?php form_password( array( 'name' => 'data[password2]', 'class' => 'st-forminput', 'id' => 'password2', 'style' => 'width: 200px', 'required' => 'true')) ?>
                                    <div class="clear"></div>
                                    </div>
                                    
                                     <div class="button-box">
                                        <?php form_submit( array( 'name' => 'submit', 'value' => 'Submit', 'id' => 'btn_submit', 'class' => 'st-button')) ?>
                                        <?php form_reset( array('name' =>'reset', 'class' => 'st-clear'), 'Clear', 'id="btn_clear"'); ?>
                                     </div>
                                    
                                    <?php form_close(); ?>
                                </div>
                             </div>
                         <!-- END SIMPLE FORM -->
                
                </div>
                <!-- END CONTENT -->      
        </div>
        <!-- END PAGE -->
        
<script type="text/javascript">
    jQuery( document).ready( function(){
        jQuery( '#form_submit').submit( function(){ 
           if( jQuery( '#password').val() != jQuery( '#password2').val() ){
            alert( 'Password not match');
                return false;
           }
        });
    });
</script>