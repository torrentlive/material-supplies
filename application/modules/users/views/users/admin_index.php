        <!-- START PAGE -->
        <div id="page">             	
                <? $this->view->partial( 'common/_admin_page_title'); ?>
                	<!-- START CONTENT -->
                    <div class="content">
                        <!-- START TABLE -->
                        <div class="simplebox grid98p">
                        
                        	<div class="titleh">
                        	  <h3>User lists</h3>
                            <div class="shortcuts-icons">
                            	<a class="shortcut tips" href="#" title="Add New User">
                                    <?php img( array( 'src' => 'images/admin/icons/shortcut/plus.png', 'alt' => 'icon')); ?>
                                </a>
                            </div>
                            </div>
                            
							<table cellpadding="0" cellspacing="0" border="0" class="display data-table" id="list-table">
								<thead>
									<tr>
                                    	<th>ID</th>
                                        <th>Email</th>
                                        <th>Created</th>
                                        <th>Modified</th>
                                        <th></th>
                                    </tr>
                               	</thead>                                
                                <tbody>   
                                    <? foreach( $users as $user): ?>                     
                                	<tr>
                                        <td><?php $user->id;?></td>
                                    	<td><?php $user->email; ?></td>
                                    	<td><?php $user->created; ?></td>
                                    	<td><?php $user->modified; ?></td>
                                        <td style="width: 80px; text-align: center;">
                                            <a href="<?php admin_user_modify_url( $user->id); ?>">
                                                <?php img( array( 'src' => '/images/admin/icons-set/modify.png', 'style' => 'height: 20px')) ?>
                                            </a>
                                            <a href="">
                                                <?php img( array( 'src' => '/images/admin/icons-set/remove.png', 'style' => 'height: 20px')) ?>
                                            </a>
                                        </td>
                                    </tr>
                                    <? endforeach; ?>                                    
								</tbody>
							</table>                     
                            
                        </div>
                        <!-- END TABLE --> 
                        <div class="clear"></div>
                    </div>
                    <!-- END CONTENT -->
        </div>
        <!-- END PAGE -->

        <!-- Start Data Tables Initialisation code -->
		<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function() {
    								jQuery('#list-table').dataTable({
        							"bJQueryUI": true,
        							"sPaginationType": "full_numbers",
                    "aoColumnDefs": [{"bSortable": false, "aTargets": [4] }],
                    "iDisplayLength": 30,
                    "aLengthMenu": [[30, 50, 100, -1], [30, 50, 100, "All"]]
        							});
    							} );
    						</script>
        <!-- End Data Tables Initialisation code -->

