        <!-- START PAGE -->
        <div id="page">           	
                <? $this->view->partial( 'common/_admin_page_title'); ?>
                <!-- START CONTENT -->
                <div class="content">

                        <!-- START SIMPLE FORM -->
                        	<div class="simplebox grid98p">
                            	<div class="titleh">
                                	<h3>User : <?php $user->email ?></h3>
                                    <div class="shortcuts-icons"></div>
                                </div>
                                <div class="body">
                                    <?php form_open( null, array( 'id' => 'form_submit')); ?>
                                    <?php form_hidden( 'data[id]', $user->id); ?>
                                  	<div class="st-form-line">	
                                    	<span class="st-labeltext">First name</span>	
                                        <?php form_input( array( 'name' => 'data[firstname]', 'class' => 'st-forminput', 'id' => 'firstname', 'style' => 'width: 200px', 'required' => 'true'), $user->firstname) ?>
                                    <div class="clear"></div>
                                    </div>
                                    
                                  	<div class="st-form-line">	
                                    	<span class="st-labeltext">Last name</span>	
                                        <?php form_input( array( 'name' => 'data[lastname]', 'class' => 'st-forminput', 'id' => 'lastname', 'style' => 'width: 200px', 'required' => 'true'), $user->lastname) ?>
                                    <div class="clear"></div>
                                    </div>
                                    
                                     <div class="button-box">
                                        <?php form_submit( array( 'name' => 'submit', 'value' => 'Submit', 'id' => 'btn_submit', 'class' => 'st-button')) ?>
                                        <?php form_reset( array('name' =>'reset', 'class' => 'st-clear'), 'Clear', 'id="btn_clear"'); ?>
                                     </div>
                                    
                                    <?php form_close(); ?>
                                </div>
                             </div>
                         <!-- END SIMPLE FORM -->
                
                </div>
                <!-- END CONTENT -->      
        </div>
        <!-- END PAGE -->
        
<script type="text/javascript">
    jQuery( document).ready( function(){
        jQuery( '#form_submit').submit( function(){ 
           if( jQuery( '#password').val() != jQuery( '#password2').val() ){
            alert( 'Password not match');
                return false;
           }
        });
    });
</script>