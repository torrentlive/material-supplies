<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users
 *
 * This model represents user authentication data. It operates the following tables:
 * - user account data,
 *
 * @package	Tank_auth
 * @author	Ilya Konyukhov (http://konyukhov.com/soft/)
 */
class User extends DataMapper{

    var $has_one = array( 'role');

	function __construct(){
		parent::__construct();

		$ci =& get_instance();
    }

	/**
	 * Get user record by Id
	 *
	 * @param	int
	 * @param	bool
	 * @return	object
	 */
	function get_user_by_id( $user_id, $activated){
		$this->where('id', $user_id);
		$this->where('activated', $activated ? 1 : 0);
        $this->get();
        
		if ( $this->result_count() == 1) return $this;
		return NULL;
	}

	/**
	 * Get user record by login (username or email)
	 *
	 * @param	string
	 * @return	object
	 */
	function get_user_by_login($login){
		$this->where('LOWER(username)=', strtolower($login));
		$this->or_where('LOWER(email)=', strtolower($login));
        $this->get($this->table_name);
		
        if ( $this->result_count() == 1) return $this;
		return NULL;
	}

	/**
	 * Get user record by username
	 *
	 * @param	string
	 * @return	object
	 */
	function get_user_by_username($username)
	{
		$this->db->where('LOWER(username)=', strtolower($username));

		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	/**
	 * Get user record by email
	 *
	 * @param	string
	 * @return	object
	 */
	function get_user_by_email($email){

        $user = $this->where( 'LOWER(email)', strtolower($email))->include_related( 'role')->get(); 

		if ($user->result_count() == 1) return $user;
		return NULL;
	}

	/**
	 * Check if username available for registering
	 *
	 * @param	string
	 * @return	bool
	 */
	function is_username_available($username){	   
        $user = $this->where( 'LOWER(username)=', strtolower($username))->get();

		return $user->result_count() == 0;
	}

	/**
	 * Check if email available for registering
	 *
	 * @param	string
	 * @return	bool
	 */
	function is_email_available($email){
        $user = $this->where( 'LOWER(email)=', strtolower($email))->or_where( 'LOWER(new_email)=', strtolower($email))->get();

		return $user->result_count() == 0;
	}

	/**
	 * Create new user record
	 *
	 * @param	array
	 * @param	bool
	 * @return	array
	 */
	function create_user($data, $activated = TRUE){
		$data['created'] = date('Y-m-d H:i:s');
		$data['activated'] = $activated ? 1 : 0;

        $this->from_array( $data);
		if ($this->save( $this->default_role())) {
			$user_id = $this->id;
			return array('user_id' => $user_id);
		}
		return NULL;
	}


	/**
	 * Get the default role for users
	 *
	 * @return	object
	 */
	 
	function default_role(){
		$this->Role->where('default', 1)->get();
        return $this->Role;
	}
	
	/**
	 * Get role for role_id
	 *
	 * @return	string
	 */
	 
	function get_role($role_id){
		$this->Role->where('id', $role_id)->get();
		return $this->Role->role;
	}
	
	/**
	 * Activate user if activation key is valid.
	 * Can be called for not activated users only.
	 *
	 * @param	int
	 * @param	string
	 * @param	bool
	 * @return	bool
	 */
	function activate_user($user_id, $activation_key, $activate_by_email){
		$this->where('id', $user_id);
		if ($activate_by_email) {
			$this->where('new_email_key', $activation_key);
		} else {
			$this->where('new_password_key', $activation_key);
		}
		$this->where('activated', 0);
		$this->get();

		if ( $this->result_count() == 1) {
			$this->activated = 1;
			$this->new_email_key = NULL;
			$this->id = $user_id;
			$this->save();
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Purge table of non-activated users
	 *
	 * @param	int
	 * @return	void
	 */
	function purge_na($expire_period = 172800){
		$this->where('activated', 0);
		$this->where('UNIX_TIMESTAMP(created) <', time() - $expire_period);
		$this->get()->delete_all();
	}

	/**
	 * Delete user record
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete_user($user_id){
		$this->id = $user_id;
		if ( $this->delete()) {
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Set new password key for user.
	 * This key can be used for authentication when resetting user's password.
	 *
	 * @param	int
	 * @param	string
	 * @return	bool
	 */
	function set_password_key($user_id, $new_pass_key){
		$this->new_password_key = $new_pass_key;
		$this->new_password_requested = date('Y-m-d H:i:s');
		$this->id = $user_id;
        return $this->save();

	}

	/**
	 * Check if given password key is valid and user is authenticated.
	 *
	 * @param	int
	 * @param	string
	 * @param	int
	 * @return	void
	 */
	function can_reset_password($user_id, $new_pass_key, $expire_period = 900){
		$this->where('id', $user_id);
		$this->where('new_password_key', $new_pass_key);
		$this->where('UNIX_TIMESTAMP(new_password_requested) >', time() - $expire_period);
        $this->get();
        
		return $this->result_count() == 1;
	}

	/**
	 * Change user password if password key is valid and user is authenticated.
	 *
	 * @param	int
	 * @param	string
	 * @param	string
	 * @param	int
	 * @return	bool
	 */
	function reset_password($user_id, $new_pass, $new_pass_key, $expire_period = 900){
		$this->password = $new_pass;
		$this->new_password_key = NULL;
		$this->new_password_requested = NULL;
		$this->id = $user_id;
		$this->where('new_password_key', $new_pass_key);
		$this->where('UNIX_TIMESTAMP(new_password_requested) >=', time() - $expire_period);

		return $this->save();
	}

	/**
	 * Change user password
	 *
	 * @param	int
	 * @param	string
	 * @return	bool
	 */
	function change_password($user_id, $new_pass){
		$this->password = $new_pass;
		$this->id = $user_id;

		return $this->save();
	}

	/**
	 * Set new email for user (may be activated or not).
	 * The new email cannot be used for login or notification before it is activated.
	 *
	 * @param	int
	 * @param	string
	 * @param	string
	 * @param	bool
	 * @return	bool
	 */
	function set_new_email($user_id, $new_email, $new_email_key, $activated){
	   
       if( !empty($activated)) $this->new_email = $new_email; else $this->email = $new_email;
		$this->new_email_key = $new_email_key;
		$this->id = $user_id;
		$this->where('activated', $activated ? 1 : 0);

		return $this->save();
	}

	/**
	 * Activate new email (replace old email with new one) if activation key is valid.
	 *
	 * @param	int
	 * @param	string
	 * @return	bool
	 */
	function activate_new_email($user_id, $new_email_key){
		$this->email = 'new_email';
		$this->new_email = NULL;
		$this->new_email_key = NULL;
		$this->id = $user_id;
		$this->where('new_email_key', $new_email_key);

		return $this->save();
	}

	/**
	 * Update user login info, such as IP-address or login time, and
	 * clear previously generated (but not activated) passwords.
	 *
	 * @param	int
	 * @param	bool
	 * @param	bool
	 * @return	void
	 */
	function update_login_info($user_id, $record_ip, $record_time){

        $this->id = $user_id;
        $this->new_password_key = null;
        $this->new_password_requested = null;

		//if ($record_ip)		$this->last_ip = $this->input->ip_address;
		if ($record_time)	$this->last_login = date('Y-m-d H:i:s');
                
        $this->save();
	}

	/**
	 * Ban user
	 *
	 * @param	int
	 * @param	string
	 * @return	void
	 */
	function ban_user($user_id, $reason = NULL)
	{
		$this->db->where('id', $user_id);
		$this->db->update($this->table_name, array(
			'banned'		=> 1,
			'ban_reason'	=> $reason,
		));
	}

	/**
	 * Unban user
	 *
	 * @param	int
	 * @return	void
	 */
	function unban_user($user_id)
	{
		$this->db->where('id', $user_id);
		$this->db->update($this->table_name, array(
			'banned'		=> 0,
			'ban_reason'	=> NULL,
		));
	}

}

/* End of file users.php */
/* Location: ./application/models/auth/users.php */