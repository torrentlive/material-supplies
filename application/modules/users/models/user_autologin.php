<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * User_Autologin
 *
 * This model represents user autologin data. It can be used
 * for user verification when user claims his autologin passport.
 *
 * @package	Tank_auth
 * @author	Ilya Konyukhov (http://konyukhov.com/soft/)
 */
class User_Autologin extends DataMapper{

    var $table = 'user_autologins';
    var $has_one = array( 'user');
    var $ci;

	function __construct(){
		parent::__construct();
		$this->ci =& get_instance();
	}

	/**
	 * Get user data for auto-logged in user.
	 * Return NULL if given key or user ID is invalid.
	 *
	 * @param	int
	 * @param	string
	 * @return	object
	 */
	function getcode($user_id, $key){
//		$this->db->select($this->users_table_name.'.id');
//		$this->db->select($this->users_table_name.'.username');
//		$this->db->from($this->users_table_name);
//		$this->db->join($this->table_name, $this->table_name.'.user_id = '.$this->users_table_name.'.id');
//		$this->db->where($this->table_name.'.user_id', $user_id);
//		$this->db->where($this->table_name.'.key_id', $key);
//		$query = $this->db->get();
//		if ($query->num_rows() == 1) return $query->row();
//		return NULL;
        
        
        $this->where( array('user_id' => $user_id, 'key_id' => $key))->include_related( 'user')->get();        
        $this->check_last_query();
        exit();
        
	}

	/**
	 * Save data for user's autologin
	 *
	 * @param	int
	 * @param	string
	 * @return	bool
	 */
	function setcode($user_id, $key){
	   $data = array(
			'user_id' 		=> $user_id,
			'key_id'	 	=> $key,
			'user_agent' 	=> substr($this->ci->input->user_agent(), 0, 149),
			'last_ip' 		=> $this->ci->input->ip_address(),
		);

         $this->from_array( $data);
         return $this->save();
	}

	/**
	 * Delete user's autologin data
	 *
	 * @param	int
	 * @param	string
	 * @return	void
	 */
	function deletecode($user_id, $key){
		$this->where('user_id', $user_id);
		$this->where('key_id', $key);
		$this->delete();
	}

	/**
	 * Delete all autologin data for given user
	 *
	 * @param	int
	 * @return	void
	 */
	function clear( $user_id = null){        
	   if( !empty( $user_id))
            $this->where('user_id', $user_id)->get()->delete();
        
	}

	/**
	 * Purge autologin data for given user and login conditions
	 *
	 * @param	int
	 * @return	void
	 */
	function purge($user_id){
       $this->where( array('user_id' => $user_id, 'user_agent' => substr( $this->ci->input->user_agent(), 0, 149), 'last_ip' => $this->ci->input->ip_address()));
       $this->get()->delete_all();

	}
}

/* End of file user_autologin.php */
/* Location: ./application/models/auth/user_autologin.php */