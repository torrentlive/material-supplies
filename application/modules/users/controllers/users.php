<? if( !defined( 'BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller{
    
    var $uses = array( 'User', 'Role');
    var $helpers = array(); 

    function __construct(){
        $this->allowPage = array();
        parent::__construct();    
    }    
    
    function admin_index(){
        $this->view->config( array( 'title' => ''));
        $this->view->set( 'layout', 'admin');
        $this->_data['page_title'] = 'Users';
        $this->_data['adminMenu'] = 'menuAdminUser';
        
        $this->_data['users'] = $this->User->where_related_roles( 'role !=', 'admin')->get();
        
        $this->view->render( $this->_data);
    }
    
    /**
     * 
     */
     function admin_profile(){
        $this->view->config( array( 'title' => ''));
        $this->view->set( 'layout', 'admin');
        $this->_data['page_title'] = 'Users';
        
        if( $this->data){ 
            if( $this->tank_auth->change_password( $this->data['oldpassword'], $this->data['password']))
                $this->FlashMessage( 'Operation Success', 'succes');
            else
                $this->FlashMessage( 'Operation Error.', 'error');
            
            redirect( $this->input->server( 'HTTP_REFERER'));
        }        
        
        $this->view->render( $this->_data);
     }
     
     /**
      * 
      */
     function admin_modify( $id = null){
        $this->view->config( array( 'title' => ''));
        $this->view->set( 'layout', 'admin');
        $this->_data['page_title'] = 'Users';
        $this->_data['adminMenu'] = 'menuAdminUser';
        
        if( $this->data){
            if( $this->User->save_upload( $this->data))
                $this->FlashMessage( 'Operation Success', 'succes');
            else
                $this->FlashMessage( 'Operation Error.', 'error');
                
            redirect( $this->input->server( 'HTTP_REFERER'));
        }
        
        $this->_data['user'] = $this->User->where( 'id', $id)->get_upload();
        
        $this->view->render( $this->_data);
     }
    
    

//
}
?>