<?php if(!defined('BASEPATH')) die('Permission deny');

class Categories extends MY_Controller{

	var $uses = array('Category');

	function __construct(){
		$this->allowPage = array();
		parent::__construct();
	}

	function admin_index(){

		$this->_data['categories'] = array();

		$this->view->set('layout', 'admin');
		$this->view->render($this->_data);
	}

}
