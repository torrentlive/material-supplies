<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Login_attempts
 *
 * This model serves to watch on all attempts to login on the site
 * (to protect the site from brute-force attack to user database)
 *
 * @package	Tank_auth
 * @author	Ilya Konyukhov (http://konyukhov.com/soft/)
 */
class Login_attempt extends DataMapper{

    var $table = 'login_attempts';

	function __construct(){
		parent::__construct();
		$ci =& get_instance();

	}

	/**
	 * Get number of attempts to login occured from given IP-address or login
	 *
	 * @param	string
	 * @param	string
	 * @return	int
	 */
	function get_attempts_num($ip_address, $login){        
		$this->where('ip_address', $ip_address);
		if (strlen($login) > 0) $this->or_where('login', $login);

		return $this->get()->result_count();
	}

	/**
	 * Increase number of attempts for given IP-address and login
	 *
	 * @param	string
	 * @param	string
	 * @return	void
	 */
	function increase_attempt($ip_address, $login){
	   $this->id = null;
        $this->ip_address = $ip_address;
        $this->login = $login;
        $this->save();
	}

	/**
	 * Clear all attempt records for given IP-address and login.
	 * Also purge obsolete login attempts (to keep DB clear).
	 *
	 * @param	string
	 * @param	string
	 * @param	int
	 * @return	void
	 */
	function clear_attempts($ip_address, $login, $expire_period = 86400){
		$this->group_start()->where(array('ip_address' => $ip_address, 'login' => $login))->group_end();
		$this->or_where('UNIX_TIMESTAMP(time) <', time() - $expire_period);
        $this->get()->delete_all();
	}
}

/* End of file login_attempts.php */
/* Location: ./application/models/auth/login_attempts.php */