<?


class Test_upload extends DataMapper{
    
    var $table = 'test_uploads';
    var $uploads = array(
        'file' => array(
            'extensions' => 'gif|jpg|png',
            'resize' => array(
                '100x100' => array(
                    'width' => 100,
                    'height' => 100,
                    'quality' => '90%'
                ),
                '300x300' => array(
                    'width' => 300,
                    'height' => 300,
                    'quality' => '90%'
                )
            )           
        ),
        'file2' => array(
            'extensions' => 'gif|jpg|png',
            'resize' => array(
                '120x100' => array(
                    'width' => 120,
                    'height' => 100,
                    'quality' => '90%'
                )
            )
        ),
    ); 
    
    var $slug = array(
        'title' => 'url'
    );
    
    function __construct(){
        parent::__construct();
    }
    
    
} 



?>