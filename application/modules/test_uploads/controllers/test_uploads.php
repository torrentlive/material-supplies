<?


class Test_uploads extends MY_Controller{
    
    var $uses = array( 'Test_upload');

    function __construct(){
        parent::__construct();
    }
    
    function index(){
        $this->view->set( 'layout', 'upload');
        
        if( $this->data)     
            $this->Test_upload->save_upload( $this->data);

        $this->_data['data'] = $this->Test_upload->where( 'id', 2)->get_upload();

        $this->view->render( $this->_data);
    }
    
    function del( $id){
        
        $this->Test_upload->delete_upload( $id);
        
    }
    
    
    
    
}


?>